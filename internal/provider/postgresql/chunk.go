package postgresql

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/serkurnikov/amazon-storage/internal/domain"
)

func (r *Repo) CreateChunk(ctx context.Context, f *domain.Chunk) (uint32, error) {
	var q = `
		insert into chunks(file_id, serial_number_id, uuid, cluster_id)
		values ($1, $2, $3, $4)
		returning id
	`
	var id uint32
	var err = r.db.QueryRowContext(ctx, q, f.FileID, f.SerialNumberID, f.UUID, f.ClusterID).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, err
}

func (r *Repo) GetChunksByFileID(ctx context.Context, fileID, clusterID uint32) ([]domain.Chunk, error) {
	var results []domain.Chunk

	query := `
		select uuid, serial_number_id, cluster_id from chunks where file_id = $1 and cluster_id = $2
	`

	rows, err := r.db.QueryContext(ctx, query, fileID, clusterID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to query database")
	}
	defer rows.Close()

	for rows.Next() {
		var result domain.Chunk
		if err := rows.Scan(&result.UUID, &result.SerialNumberID, &result.ClusterID); err != nil {
			return nil, errors.Wrap(err, "failed to scan row")
		}
		result.FileID = fileID
		results = append(results, result)
	}

	if err := rows.Err(); err != nil {
		return nil, errors.Wrap(err, "error reading rows")
	}

	return results, nil
}
