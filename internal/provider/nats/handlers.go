package nats

import (
	"fmt"
	"time"

	"gitlab.com/serkurnikov/amazon-storage/config"
	"gitlab.com/serkurnikov/amazon-storage/internal/provider/nats/internal/subscription"
)

const (
	BServerCluster = "b"
)

type subscribeHandler struct {
	handler      func(*Subscriber) subscription.Handler
	stream       string
	consumer     string
	subject      string
	workers      int
	pullInterval time.Duration
	timeout      time.Duration
}

func createHandlers(cfg config.Config, clusterID uint32) []subscribeHandler {
	handlers := []subscribeHandler{
		{
			stream:       "files",
			consumer:     fmt.Sprintf("files-%s%v-upload", BServerCluster, clusterID),
			subject:      fmt.Sprintf("%s%s%v.upload", filesSubject, BServerCluster, clusterID),
			workers:      cfg.Nats.SubscriptionWorkers,
			pullInterval: 50 * time.Millisecond,
			timeout:      time.Minute * 10,
			handler:      chunkFileSaverHandler,
		},
		{
			stream:       "files",
			consumer:     fmt.Sprintf("files-%s%v-fetch", BServerCluster, clusterID),
			subject:      fmt.Sprintf("%s%s%v.fetch", filesSubject, BServerCluster, clusterID),
			workers:      cfg.Nats.SubscriptionWorkers,
			pullInterval: 50 * time.Millisecond,
			timeout:      time.Minute * 10,
			handler:      chunkFileGetterHandler,
		},
	}

	return handlers
}
