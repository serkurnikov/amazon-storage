package subscription

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	"github.com/powerman/structlog"
)

var workerPullCheckPeriod = 50 * time.Millisecond

type Handler func(ctx context.Context, msg *nats.Msg) error

type Subscription struct {
	js      nats.JetStreamContext
	sub     *nats.Subscription
	workers chan struct{}
	doneCh  chan struct{}
	logger  *structlog.Logger
	handler Handler

	closeCh                   chan struct{}
	ticker                    *time.Ticker
	stream, consumer, subject string
	nWorkers                  int

	pullInterval time.Duration

	gracePeriod time.Duration

	mu      sync.Mutex
	running bool
}

func NewSubscription(
	js nats.JetStreamContext,
	logger *structlog.Logger,
	stream, consumer, subject string,
	pullInterval, gracePeriod time.Duration,
	nWorkers int,
	handler Handler,
) (*Subscription, error) {
	if nWorkers <= 0 {
		return nil, errors.New("should be at least one worker to create Subscription")
	}

	return &Subscription{
		logger:       logger,
		js:           js,
		stream:       stream,
		consumer:     consumer,
		subject:      subject,
		handler:      handler,
		pullInterval: pullInterval,
		gracePeriod:  gracePeriod,
		nWorkers:     nWorkers,
	}, nil
}

func (s *Subscription) Start(ctx context.Context) error {
	s.mu.Lock()
	if s.running {
		s.mu.Unlock()

		return errors.WithStack(fmt.Errorf(
			"subscriber for stream %q and subject %q is already running",
			s.stream,
			s.subject,
		))
	}

	sub, err := s.js.PullSubscribe(s.subject, s.consumer, nats.Bind(s.stream, s.consumer), nats.ManualAck())
	if err != nil {
		s.mu.Unlock()

		return errors.Wrapf(
			err,
			"couldn't subscribe to consumer %q on stream %q an subject %q",
			s.stream,
			s.consumer,
			s.subject,
		)
	}
	s.sub = sub
	s.ticker = time.NewTicker(s.pullInterval)
	s.doneCh = make(chan struct{})
	s.closeCh = make(chan struct{})
	s.workers = make(chan struct{}, s.nWorkers)
	for i := 0; i < s.nWorkers; i++ {
		s.workers <- struct{}{}
	}
	s.running = true
	s.mu.Unlock()

	go func() {
		ctxTerminate, terminate := context.WithCancel(ctx)
		defer terminate()

		for {
			select {
			case <-s.ticker.C:
				nWorkers := len(s.workers)
				if nWorkers == 0 {
					continue
				}
				msgs, err := s.sub.Fetch(nWorkers)
				if err != nil && !errors.Is(err, nats.ErrTimeout) {
					s.logger.PrintErr("couldn't fetch msgs for", "subject", s.subject, "err", err)
					continue
				}

				for _, msg := range msgs {
					go func(ctx context.Context, msg *nats.Msg) {
						<-s.workers
						defer func() {
							s.workers <- struct{}{}
						}()
						_ = s.handler(ctxTerminate, msg)
					}(ctx, msg)
				}

			case <-s.closeCh:
				if err := s.sub.Unsubscribe(); err != nil {
					s.logger.Printf("couldn't unsubscribe from stream %q and subject %q err %v\n", s.stream, s.subject, err)
				}

				for len(s.workers) < s.nWorkers {
					time.Sleep(workerPullCheckPeriod)
				}

				close(s.doneCh)

				return
			}
		}
	}()

	return nil
}

func (s *Subscription) Stop() error {
	s.mu.Lock()
	if !s.running {
		s.mu.Unlock()

		return nil
	}
	s.ticker.Stop()
	s.mu.Unlock()

	close(s.closeCh)

	defer func() {
		s.mu.Lock()
		s.running = false
		s.mu.Unlock()
	}()

	select {
	case <-s.doneCh:
		return nil
	case <-time.After(s.gracePeriod):
		return errors.WithStack(fmt.Errorf(
			"subscriber for stream %q and subject %q wasn't gracefully stopped",
			s.stream,
			s.subject,
		))
	}
}
