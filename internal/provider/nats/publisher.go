package nats

import (
	"context"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/nuid"
	"github.com/pkg/errors"
	"github.com/powerman/structlog"
	"gitlab.com/serkurnikov/amazon-storage/internal/domain"
)

type Publisher struct {
	logger       *structlog.Logger
	js           nats.JetStreamContext
	ClusterID    uint32
	nuid         *nuid.NUID
	filesSubject string
	filesOpts    []nats.PubOpt
}

func NewPublisher(
	js nats.JetStreamContext,
	clusterID uint32,
	logger *structlog.Logger,
) *Publisher {
	return &Publisher{
		js:           js,
		ClusterID:    clusterID,
		logger:       logger,
		nuid:         nuid.New(),
		filesSubject: "files.a.fetch",
		filesOpts:    []nats.PubOpt{},
	}
}

func (p *Publisher) SendChunk(_ context.Context, chunk domain.ChunkSaveEvent) error {
	b, err := json.Marshal(chunk)
	if err != nil {
		return errors.Wrap(err, "publisher: SendChunk couldn't marshall data")
	}
	return p.publish(p.filesSubject, b, p.nuid.Next(), p.filesOpts...)
}

func (p *Publisher) publish(subject string, data []byte, msgID string, opts ...nats.PubOpt) error {
	msg := nats.NewMsg(subject)
	msg.Data = data
	msg.Header.Set(nats.MsgIdHdr, msgID)
	_, err := p.js.PublishMsg(msg, opts...)
	if err != nil {
		p.logger.PrintErr("couldn't publish message", "subject", subject, "err", err)
		return errors.Wrapf(err, "couldn't publish message to %s", subject)
	}
	return nil
}
