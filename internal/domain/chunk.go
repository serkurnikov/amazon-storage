package domain

type Chunk struct {
	FileID         uint32
	ClusterID      uint32
	SerialNumberID uint32
	UUID           string
}
