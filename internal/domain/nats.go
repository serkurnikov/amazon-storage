package domain

type ChunkSaveEvent struct {
	RequestID      string `json:"r"`
	FileID         uint32 `json:"f"`
	SerialNumberID uint32 `json:"s"`
	Data           []byte `json:"d"`
}

type ChunkGetEvent struct {
	RequestID string `json:"r"`
	FileID    uint32 `json:"f"`
}
