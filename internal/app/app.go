package app

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/powerman/structlog"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/serkurnikov/amazon-storage/config"
	"gitlab.com/serkurnikov/amazon-storage/internal/provider/nats"
	"gitlab.com/serkurnikov/amazon-storage/internal/provider/postgresql"
	"gitlab.com/serkurnikov/amazon-storage/internal/usecase/amazonstorage"
	"gitlab.com/serkurnikov/amazon-storage/pkg/def"
	"gitlab.com/serkurnikov/amazon-storage/pkg/metric"
	"golang.org/x/sync/errgroup"
)

func StartService() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	logger := structlog.FromContext(ctx, nil)
	def.SetupLog()

	cfg := config.InitConfiguration(logger)
	logger.Println("Nats configuration", cfg.Nats)
	logger.Println("Postgres configuration", cfg.Database)

	runtime.GOMAXPROCS(cfg.Service.MaxCpuUsage)
	logger.Println("Current CPU Usage:", runtime.GOMAXPROCS(-1))

	js, err := nats.NewJetConnection(ctx, cfg.Nats, logger)
	if err != nil {
		logger.Fatalln("couldn't connect to nats", err)
	}

	repo, err := postgresql.New(&cfg.Database, logger)
	if err != nil {
		logger.Fatalln("failed init repo", err)
	}

	var (
		reg     = prometheus.NewPedanticRegistry()
		promSvc = &metric.Metric{}
	)
	metric.CreateMetrics(promSvc, reg)
	metricsSvc := metric.CreateMetricServer(cfg.Prometheus.Addr(), reg)

	eventPublisher := nats.NewPublisher(js, cfg.Service.ClusterID, logger)
	amazonStorageUs := amazonstorage.New(ctx, repo, eventPublisher, cfg, logger)
	eventSubscriber := nats.NewSubscriber(cfg, js, logger, amazonStorageUs)

	err = GracefullyServe(ctx, &cfg, amazonStorageUs, eventSubscriber, metricsSvc, logger, func(ctx context.Context) error {
		amazonStorageUs.Stop()
		if cfg.Prometheus.Enable {
			_ = metricsSvc.Shutdown(ctx)
		}
		_ = eventSubscriber.Stop(ctx)
		time.Sleep(3 * time.Second) // timeout for successful completed all workers
		_ = repo.Disconnect()
		return nil
	})
	if err != nil {
		logger.Fatal("application stopped with error", err)
	}

	logger.Info("application stopped gracefully")
}

func GracefullyServe(ctx context.Context, cfg *config.Config, farmScanUs amazonstorage.Usecase, eventSubscriber *nats.Subscriber, metricsSvc *http.Server, logger *structlog.Logger, teardown func(context.Context) error) error {
	signalChan := make(chan os.Signal, 1)
	fail := make(chan error)

	go func() {
		signal.Notify(
			signalChan,
			syscall.SIGINT,
			syscall.SIGTERM,
		)

		<-signalChan
		logger.Info("Interrupting - shutting down...")
		gracefulCtx, cancelShutdown := context.WithTimeout(ctx, cfg.Service.GracePeriod)
		defer cancelShutdown()
		fail <- teardown(gracefulCtx)
	}()

	g, ctx := errgroup.WithContext(ctx)
	if cfg.Prometheus.Enable {
		g.Go(func() error {
			logger.Info("starting prometheus metrics")
			return metricsSvc.ListenAndServe()
		})
	}

	g.Go(func() error {
		logger.Info("starting event subscriber")
		return eventSubscriber.Start(ctx)
	})

	if err := g.Wait(); err != nil {
		logger.PrintErr(err)
	}

	return <-fail
}
