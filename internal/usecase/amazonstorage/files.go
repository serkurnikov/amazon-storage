package amazonstorage

import (
	"context"
	"fmt"
	"os"

	"github.com/google/uuid"
	"gitlab.com/serkurnikov/amazon-storage/internal/domain"
)

const (
	storageDefaultFolder = "./storage"
)

func (s *storage) SendChunks(ctx context.Context, event domain.ChunkGetEvent) error {
	chunks, err := s.repo.GetChunksByFileID(ctx, event.FileID, s.cfg.Service.ClusterID)
	if err != nil {
		return err
	}

	for _, c := range chunks {
		data, err := readFileFromFolder(c.UUID)
		if err != nil {
			return err
		}

		err = s.publisher.SendChunk(ctx, domain.ChunkSaveEvent{
			RequestID:      event.RequestID,
			FileID:         c.FileID,
			SerialNumberID: c.SerialNumberID,
			Data:           data,
		})
		if err != nil {
			return err
		}
	}
	s.logger.Info("Send chunks", "fileID", event.FileID)
	return nil
}

func (s *storage) SaveChunk(ctx context.Context, chunk domain.ChunkSaveEvent) error {
	s.logger.Info("Received file part", "fileID", chunk.FileID)
	chunkUUID := uuid.New().String()
	_, err := s.repo.CreateChunk(ctx, &domain.Chunk{
		FileID:         chunk.FileID,
		ClusterID:      s.cfg.Service.ClusterID,
		SerialNumberID: chunk.SerialNumberID,
		UUID:           chunkUUID,
	})
	if err != nil {
		s.logger.PrintErr("Failed saved chunk to database", "fileID", chunk.FileID, "err", err.Error())
		return err
	}

	err = saveFileToFolder(chunk.Data, storageDefaultFolder, chunkUUID)
	if err != nil {
		s.logger.PrintErr("Failed saved chunk to folder", "fileID", chunk.FileID, "err", err.Error())
		return err
	}
	return nil
}

func saveFileToFolder(data []byte, folderPath, fileName string) error {
	// Create the folder if it doesn't exist
	if _, err := os.Stat(folderPath); os.IsNotExist(err) {
		if err := os.MkdirAll(folderPath, os.ModePerm); err != nil {
			return err
		}
	}

	// Create the file in the specified folder
	filePath := fmt.Sprintf("%s/%s", folderPath, fileName)
	err := os.WriteFile(filePath, data, 0644)
	if err != nil {
		return err
	}

	return nil
}

func readFileFromFolder(chunkUUID string) ([]byte, error) {
	// Check if the specified file exists
	filePath := fmt.Sprintf("%s/%s", storageDefaultFolder, chunkUUID)
	_, err := os.Stat(filePath)
	if os.IsNotExist(err) {
		return nil, fmt.Errorf("file not found: %s", filePath)
	} else if err != nil {
		return nil, err
	}

	// Read the contents of the file
	fileData, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	return fileData, nil
}
