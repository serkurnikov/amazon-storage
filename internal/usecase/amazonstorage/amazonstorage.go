package amazonstorage

import (
	"context"

	"github.com/powerman/structlog"
	"gitlab.com/serkurnikov/amazon-storage/config"
	"gitlab.com/serkurnikov/amazon-storage/internal/domain"
	"gitlab.com/serkurnikov/amazon-storage/internal/provider/nats"
	"gitlab.com/serkurnikov/amazon-storage/pkg/shutdown"
)

type storage struct {
	repo      Repo
	logger    *structlog.Logger
	shutdown  shutdown.Shutdown
	publisher *nats.Publisher

	cfg config.Config
}

type Repo interface {
	CreateChunk(ctx context.Context, f *domain.Chunk) (uint32, error)
	GetChunksByFileID(ctx context.Context, fileID, clusterID uint32) ([]domain.Chunk, error)
}

type Usecase interface {
	SaveChunk(context.Context, domain.ChunkSaveEvent) error
	SendChunks(ctx context.Context, event domain.ChunkGetEvent) error

	Stop()
}

func New(ctxShutdown context.Context, repo Repo, publisher *nats.Publisher, cfg config.Config, logger *structlog.Logger) Usecase {
	s := &storage{
		repo:      repo,
		publisher: publisher,
		shutdown:  shutdown.New(ctxShutdown),
		cfg:       cfg,
		logger:    logger,
	}

	return s
}

func (s *storage) Stop() {
	s.shutdown.Stop()
}
