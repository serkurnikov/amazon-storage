package metric

import (
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Metric struct {
	UniswapHandlerDuration  *prometheus.HistogramVec
	PricesHandlerDuration   *prometheus.HistogramVec
	BlockProcessingDuration *prometheus.HistogramVec
}

const (
	namespace    = "dex_scan"
	subUniswap   = "uniswap"
	subPrices    = "prices"
	subBlockProc = "block_processing"
)

const (
	UniswapHandlerDepositV2 = "deposit_v2"
)

func CreateMetrics(metric *Metric, reg *prometheus.Registry) {
	metric.UniswapHandlerDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: namespace,
			Subsystem: subUniswap,
			Name:      "uni_handler_duration",
			Help:      "Uniswap (v2/v3) handler duration.",
		},
		[]string{"handler"},
	)
	reg.MustRegister(metric.UniswapHandlerDuration)

	metric.PricesHandlerDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: namespace,
			Subsystem: subPrices,
			Name:      "price_handler_duration",
			Help:      "Price handler duration.",
		},
		[]string{},
	)
	reg.MustRegister(metric.PricesHandlerDuration)

	metric.BlockProcessingDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: namespace,
			Subsystem: subBlockProc,
			Name:      "block_processing_duration",
			Help:      "Block processing duration during scanning",
		},
		[]string{"mode"},
	)
	reg.MustRegister(metric.BlockProcessingDuration)
}

func CreateMetricServer(addr string, reg *prometheus.Registry) *http.Server {
	reg.MustRegister(prometheus.NewGoCollector())
	handler := promhttp.InstrumentMetricHandler(reg, promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
	mux := http.NewServeMux()
	mux.Handle("/metrics", handler)
	return &http.Server{
		Addr:              addr,
		Handler:           handler,
		ReadHeaderTimeout: time.Second,
	}
}
