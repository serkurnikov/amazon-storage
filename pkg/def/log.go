package def

import (
	"log"
	"strings"
	"time"

	"github.com/powerman/structlog"
)

type PrinterFunc structlog.PrinterFunc

var printer PrinterFunc

// Log field names.
const (
	LogRemote     = "remote"
	LogServer     = "server" // "OpenAPI", "gRPC", "Prometheus metrics", etc.
	LogGRPCCode   = "grpcCode"
	LogRemoteIP   = "remoteIP"   // IP address.
	LogAddr       = "addr"       // host:port.
	LogHost       = "host"       // DNS hostname or IPv4/IPv6 address.
	LogPort       = "port"       // TCP/UDP port number.
	LogHTTPMethod = "httpMethod" // GET, POST, etc.
	LogHTTPStatus = "httpStatus" // Status code: 200, 404, etc.
	LogFunc       = "func"       // RPC/event handler method name, Rest resource path.
	LogUserID     = "userID"
)

func SetupLog() {
	structlog.DefaultLogger.
		SetPrefixKeys(
			structlog.KeyLevel,
			structlog.KeyTime,
			LogFunc, structlog.KeyFunc,
		).
		SetDefaultKeyvals(
			structlog.KeyTime, structlog.Auto,
		).
		SetSuffixKeys(
			structlog.KeyStack, structlog.KeySource,
		).
		SetKeysFormat(map[string]string{
			structlog.KeyTime:   " %[2]s",
			structlog.KeyStack:  " %6[2]s",
			structlog.KeySource: " %6[2]s",
			structlog.KeyUnit:   " %6[2]s",
			LogGRPCCode:         " %-16.16[2]s",
			LogFunc:             " %[2]s:",
			"duration":          " %[2]q",
			"request":           " %[1]s=% [2]X",
			"response":          " %[1]s=% [2]X",
		}).
		SetTimeFormat(time.Stamp).
		SetPrinter(printer)
}

func (p PrinterFunc) Print(v ...interface{}) {
	level := strings.TrimSpace(v[0].(string))

	data := make([]interface{}, 0)
	for _, value := range v {
		data = append(data, strings.TrimSpace(value.(string)))
	}

	values := make([]interface{}, 0)
	values = append(values, data[1], data[len(v)-1])
	values = append(values, data[2:len(data)-1]...)

	logging(level, values...)
}

func logging(level string, v ...interface{}) {
	l := structlog.ParseLevel(level)
	pre := make([]string, 0)
	switch l {
	case structlog.INF:
		pre = []string{"\033[36m", "INF:", "\033[0m"}
	case structlog.WRN:
		pre = []string{"\033[33m", "WAR:", "\033[0m"}
	case structlog.ERR:
		pre = []string{"\033[31m", "ERR:", "\033[0m"}
	case structlog.DBG:
		pre = []string{"\033[37m", "DBG:", "\033[0m"}
	}

	preI := make([]interface{}, len(pre))
	for i, s := range pre {
		preI[i] = s
	}
	v = append(preI, v...)
	log.Println(v...)
}
