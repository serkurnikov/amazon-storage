package main

import "gitlab.com/serkurnikov/amazon-storage/internal/app"

func main() {
	app.StartService()
}
